# Jupyter with ProxPython toolbox

A JupyterLab docker image with a preinstalled (modified) version of the [ProxPython](https://gitlab.gwdg.de/nam/ProxPython) library.

Modifications are mainly focused on making the toolbox work in the new folder structure.

## Running the Image

You need to have Docker installed on your system. A download can be found at https://www.docker.com.

To run use the command `docker-compose up`. A first start might take a long time while all required libraries are downloaded.

When the script finished a JupyterLab server should be available on `http://localhost:8888/`.

## Options

To set a token for authentification you can change the option within the docker-compose.yml file.