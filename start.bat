@echo off

netstat -o -n -a | findstr 0.0:8888

IF %ERRORLEVEL% equ 0 (
    @echo "There is already a program running on port 8888!"
    @echo "Try checking if the program is already running"
    PAUSE
) ELSE (
    docker-compose up
)
